from view_obj import View
from model import Ensemble
from model import Person

class Controller():
    def __init__(self):
        self.view = View(self)
        self.model = Ensemble()
    
    def start_view(self):
        self.view.create_fields()
        self.view.main()

    def delete(self):
        if self.view.ask_question() ==1:
            person = Person(self.view.get_value("Nom"),
                        self.view.get_value("Prenom"),
                        self.view.get_value("Telephone"),
                        self.view.get_value("Adresse"),
                        self.view.get_value("Ville"))

            test = Person(self.view.delete_value("Nom"),
                        self.view.delete_value("Prenom"),
                        self.view.delete_value("Telephone"),
                        self.view.delete_value("Adresse"),
                        self.view.delete_value("Ville"))
            self.model.delete_person(person)
            print(person)
            self.view.question()

    def insert(self):
        person = Person(self.view.get_value("Nom"),
            self.view.get_value("Prenom"),
            self.view.get_value("Telephone"),
            self.view.get_value("Adresse"),
            self.view.get_value("Ville"))
        print(person)
        self.view.boxinsertion()
        self.model.insert_person(person)

    def search(self):
        rename = self.view.get_value("Nom")
        valuee = self.model.search_person(rename)
        if len(valuee) == 0:
            self.view.error_box()
        elif len(valuee) == 1:
            for person in valuee:
                b = person.get_prenom()
                self.view.set_value("Prenom", b)
                c = person.get_telephone()
                self.view.set_value("Telephone", c)
                d = person.get_adresse()
                self.view.set_value("Adresse", d)
                e = person.get_ville()
                self.view.set_value("Ville", e)
                self.view.boxinsertion()
        else:
            self.view.error_box()

    def quit(self):
        self.view.quit()

    def button_press_handle(self, buttonId):
        print("[Controller][button_press_handle] "+ buttonId)
        if buttonId == "Chercher":
            self.search()
        elif buttonId == "Effacer":
            self.delete()
        elif buttonId == "Inserer":
            self.insert()
        elif buttonId == "Quitter":
            self.quit()
        else:
            pass

if __name__ == "__main__":
    controller = Controller()
    controller.start_view()