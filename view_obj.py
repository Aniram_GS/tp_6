# coding: utf-8

import tkinter as tk
from tkinter import Button
from tkinter import Entry
from tkinter import Label
from tkinter import StringVar
from tkinter import Tk
from tkinter import messagebox

"""
Class View of the project

@author : Olivier CHABROL
"""

class View(Tk):
    def __init__(self, controller):
        super().__init__()
        self.controller = controller
        self.widgets_labs = {}
        self.widgets_entry = {}
        self.widgets_button = {}
        self.entries = ["Nom", "Prenom", "Telephone", "Adresse", "Ville"]
        self.buttons = ["Chercher", "Inserer", "Effacer", "Quitter"]
        self.modelListFields = []
        self.fileName = None
        self.windows ={}
        self.windows["fenetreResult"] = ...
        self.windows["fenetreErreur"] = ...

    def get_value(self, key):
        return self.widgets_entry[key].get()

    def set_value(self, key, infos):
        print(self.widgets_entry[key])
        self.widgets_entry[key].insert(0, infos)

    def error_box(self):
        return messagebox.showerror('Error', "Il n'y a pas assez d'informations")

    def boxinsertion(self):
        return messagebox.showinfo('Information', 'Les valeurs ont bien été ajoutées/ enregistrées')

    def question(self):
        return messagebox.showinfo('Information', 'Les valeurs ont bien été effacées')

    def ask_question(self):
        answer = messagebox.askokcancel('Suppression', 'Voulez-vous effacer les valeurs ?')
        if answer:
            return 1

    def delete_value(self, key):
        return self.widgets_entry[key].delete(0, tk.END)

    def quit(self):
        answer = messagebox.askokcancel('Suppression', 'Voulez-vous quitter ?')
        if answer:
            return self.destroy()

    def create_fields(self):
        i, j = 0, 0

        for idi in self.entries:
            lab = Label(self, text=idi.capitalize())
            self.widgets_labs[idi] = lab
            lab.grid(row=i,column=0)

            var = StringVar()
            entry = Entry(self, text=var)
            self.widgets_entry[idi] = entry
            entry.grid(row=i,column=1)

            i += 1

        for idi in self.buttons:
            buttonW = Button(self, text = idi, command=(lambda button=idi: self.controller.button_press_handle(button)))
            self.widgets_button[idi] = buttonW
            buttonW.grid(row=i+1,column=j)
            #self.widgets_button[idi].config(command = idi)

            j += 1


    def main(self):
        print("[View] main")
        self.title("Annuaire")
        self.mainloop()